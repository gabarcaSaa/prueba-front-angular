import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'


const baseUrl = "http://patovega.com/prueba_frontend/"
@Injectable({
  providedIn: 'root'
})
// Servicio encargado de recibir la data y devolverla como una promesa
export class BackendService {

  constructor(private http: HttpClient) { }

  public get(url: string): Observable<any> {
    try {
      return this.http.get(`${baseUrl}${url}`);
    } catch (error) {
      Promise.reject(error)
    }
  }


}

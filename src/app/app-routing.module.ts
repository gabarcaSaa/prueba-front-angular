import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstViewComponent } from './views/first-view/first-view.component';
import { SecondViewComponent } from './views/second-view/second-view.component';

const routes: Routes = [
  { path: '', component: FirstViewComponent },
  { path: 'second', component: SecondViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

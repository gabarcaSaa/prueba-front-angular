import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../services/backend.service'
import { Router } from '@angular/router';

const url = "dict.php"
@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.css']
})

export class SecondViewComponent implements OnInit {

  constructor(private backendSrv: BackendService, public router: Router) { }
  //Array list se encarga de guardar la data procesada
  list: any[] = []
  loading = false
  abecedario: string[] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "u", "v", "w", "x", "y", "z"]
  ngOnInit(): void {
  }

  //Funcion que se ejecuta al presionar el boton.

  clickButton(): void {
    this.list = []
    this.loading = true
    this.backendSrv.get(url).subscribe(result => {
      this.processData(JSON.parse(result["data"]))
    }, error => {
      window.alert("No se pudo obtener la data.")
      this.loading = false
    })
  }

  //Funcion encargada de procesar la data, se utilizaron expresiones regulares, para el letra por letra 
  //y numeros contenidos

  processData(data: object[]): void {
    if (!data || data.length === 0) {
      window.alert("No vienen datos!")
    } else {
      for (let paragraph of data) {
        let result = {}
        for (let letter of this.abecedario) {
          let regexp = RegExp(letter, "gm")
          let sum = paragraph['paragraph'].toLowerCase().match(regexp)
          if (sum) result[letter] = sum.length
          else result[letter] = 0
        }
        let total = paragraph['paragraph'].match(/\d+/g)    //\d+ expresion que encuentra numeros.
        if (total) result['total'] = total.reduce((a, b) => parseInt(a) + parseInt(b), 0)
        else result['total'] = 0
        this.list.push(result)

      }
      this.loading = false
    }

  }

}

import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../services/backend.service'
import { Router } from '@angular/router';

const url = "array.php"

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.css']
})


export class FirstViewComponent implements OnInit {
  constructor(private backendSrv: BackendService, public router: Router) { }
  list: any[] = []
  orderList: number[] = []
  textBox: String = ""
  loading = false

  ngOnInit(): void {

  }

  clickButton(): void {
    this.loading = true
    this.list = []
    this.backendSrv.get(url).subscribe(result => {
      this.addData(result)
      this.sortArray(result['data'])
    }, error => {
      window.alert("No se pudo obtener la data.")
      this.loading = false
    })
  }

  //Función encargada de agregar la data lista para consumir en la tabla, guardando los datos en la variable list
  addData(data: object): void {
    let arrayAux = []

    if (!data['data'] || data['data'].length === 0) {
      window.alert("No existen numeros")
    } else {
      for (let number of data['data']) {
        if (arrayAux.indexOf(number) === -1) {
          this.list.push({
            number: number,
            quantity: this.countNumber(data['data'], number),
            first: data['data'].indexOf(number),
            last: data['data'].lastIndexOf(number)
          })
          arrayAux.push(number)
        }
      }
      this.loading = false
    }
  }
  //Función encargada de ordenar de menor a mayor los numeros
  sortArray(data: number[]): void {
    let result = data
    let temp
    let indexRep = []
    for (let i = 0; i < result.length; i++) {
      for (let j = i + 1; j < result.length; j++) {
        if (result[i] === result[j]) indexRep.push(result[j])
        if (result[i] > result[j]) {
          temp = result[i]
          result[i] = result[j]
          result[j] = temp
        }
      }
    }
    this.textBox = result.join(' - ')
  }


  //Función para contar cuantas veces se repite un valor

  countNumber(data: number[], number: number): number {
    let count = -1
    for (let num of data) {
      if (num === number) {
        count += 1
      }
    }
    return count
  }

}
